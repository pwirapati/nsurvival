#
# nplot.survfit: extension to plot.survfit
#
# DONE
# - number of events shouldn't be aligned with the margin [20141020]
# - group variable name(s) should be shown above [20141020]
#
# TODO:
# - own plotting function
# - confidence bars (depends on ticks location, take logical vector for
#   each strata)
# - the fontsize of the number-at-risk, HRcomparison, curve labels.
#   There should be minimial tweaking here. At most, tie it to cex.axis.
# - mar.min: use these as lower limits (automatic one can't be smaller).
#   the default should be the current value to allow "inheriting" the
#   setting in multi-panel context
# - total
# - survival [CI] at xmax (if such is used), NA for terminated follow-up
# - automatic xaxis tick spacing (and yaxis, too)
# - many of the graphic tricks should be refactored into `ngraphics`
#

kmplot <- function( 
	S, 												# a `survfit` object
	glab = NULL,							# user-specified group labels
	las=1,                    # make all tick labels read horizontally
	gtitle = NULL,
	nrtitle = "number at risk:",
	evtitle = "events:",
	xunit = "",
	HRcomparison = NULL,
  HRint = NULL,
  HRtext = NULL,
	HRtextside = 3,
	HRtextat = 0.33*xmax,
  HRgroupcmp = 1,           # 0: dont print, 1: no newline, 2: with newline
  HRestCI = 1,              # 0: dont print, 1: no newline, 2: with newline
  HRpvalue = 1,             # 0: dont print, 1: no newline, 2: with newline
  HRpadj = 0,
 	
	mar.add=c(1,1,1,1),       # extra margin (in lines)
	pad.left="M",           	# padding between group label and no. at risk (in "em")
	pad.right="M",

	col = 1,									# colors
  xscale = 1,               # rescale the original unit
	yscale = 1,
	main = "",                # plot label
	xmax = NULL,              # right-bound of horizontal axis, in xscale unit!
														# Note: xmax behaves differently from `plot.survfit`
	ylim =c(0,1),
	ylab = "survival",
	mark = "/",
	... )
{
	require(survival)

	ngroup <- max( 1, length(S$strata) )
	
	if(!missing(col))
		col <- array( col, ngroup )  # recycling rule

	if( missing(glab) )
		{
		glab <- if( ngroup > 1) attr( S$strata,"names" ) else "total"
	 	glab	<- gsub("[^,=]*=","",glab ) # strip variable names
    glab <- gsub(",","/",gsub(" *$","",gsub("^ *","",glab)))

    if(is.null(gtitle))
      {
      gtitle <- if( ngroup > 1) attr( S$strata,"names" )[1] else "group"
      gtitle <- paste0( gsub("=[^,]*", "", gtitle), ":") # strip values
      gtitle <- gsub(", ","/",gtitle)
      }
		}

	if( is.null(xmax) ) xmax <- max( S$time )/xscale

	# The left margin should fit `glab` plus nrisk at time zero and some padding.
	# It should not be smaller than 4 lines (default for y-axis labels).
	#
	# first make the concatenated strings for all lines:
  message("ngroup = ",ngroup)
	n.risk0 <- sapply( 1:ngroup, function(i)
											 summary( if(ngroup > 1) S[i] else S,time=0)$n.risk )
	str.glab <- paste0( pad.left, c( gtitle, glab ) )
	widest.glab <- str.glab[ which.max( strwidth(str.glab, u="i")) ]

	widest.nrisk0 <- n.risk0[ which.max( strwidth( n.risk0, u="i" ) ) ]

	widest.left <- paste0( widest.glab, widest.nrisk0 )

	# The right margin
	#
	t.events <- xscale * xmax
	n.events <- sapply( 1: ngroup, function(i){
													si <- if(ngroup > 1) S[i] else S
														sum( si$n.event[ si$time <= t.events ] )
											 		} )
	str.events <- c(evtitle, n.events)
	widest.events <- str.events[ which.max( strwidth( str.events,u="i")) ]

  medsurv <- apply(
      rbind(sapply(quantile(S,0.5),function(v) sprintf("%4.1f",v))),
    1, function(u)
    paste0(u[1]," [",u[2],",",u[3],"]"))
  widest.medsurv <- medsurv[ which.max( strwidth( medsurv,u="i" ) ) ]

	str.right <- paste0( pad.right,
    c(xunit, paste(widest.events,widest.medsurv),widest.glab) )
	widest.right <- str.right[ which.max( strwidth( str.right, u="i" ) ) ]

	# setup the unit of margins in 'lines'
	#
	lh <- strheight("\n",u="i") - strheight("",u="i")	  # line height (in inches)

  # plot margins
  #
	par(mar = array( mar.add, 4 ) 
				+ c( 
					4 + ngroup + 1, 														# bottom
					max( 4, strwidth(widest.left,u="i")/lh ), 	# left
				  if(!missing(main) && main != "" ) 2 else 0, # top
					strwidth(paste(widest.right), u="i")/lh     # right
					))
	
	
	par(las=las)
	frame()
	plot.window(xlim=c(0,xmax),ylim=ylim,xaxs="r") 
	lines( S, col=col, yscale=yscale,xscale=xscale, xmax=xscale*xmax,
			mark=mark, ylim=ylim, ... )
	axis(side=1)
	axis(side=2,at=seq(0,1,.2),labels=c("0","20","40","60","80","100"),las=2)
	title(main=main,ylab=ylab)
	
	# group labels for number at risk
	# 
	# this is tricky because `at` for `mtext` take user coord, which changes if
	# the plot window is resized
	#
	recordGraphics(
		{
		at = ref - xinch(winch)
		mtext( labs, side=1, line=2+(1:length(labs)), at = at, col=cols,
				 	adj=0, cex = par("cex") )
		},
		list( labs=c(gtitle, glab),
				  ref=par("usr")[1],
					winch = strwidth(widest.left, u="i"),
				  cols = c("black",col) ),
		getNamespace("graphics")
	)

	# number of events
	#
	recordGraphics(
		{
		at = ref + xinch(winch)
		mtext( labs, side=1, line=2+(1:length(labs)), at = at, col= cols,
				 	adj=1, cex = par("cex") )
		mtext( xunit, line=1, adj=1, at=at, side=1, cex = par("cex"))
		},
		list( labs=c( evtitle, n.events), 
				 ref = par("usr")[2],
				 winch = strwidth( paste0(pad.right, widest.events), u="i"),
				 xunit=xunit,
			 	 cols=c("black",col) ),
		getNamespace("graphics")
	)

  # median survival [CI]
  #
  recordGraphics(
    {
    at = ref + xinch(winch)
    mtext( labs, side=1, line=2+(1:length(labs)), at = at, col = cols,
      adj=0, cex=par("cex"))
    },
    list ( labs=c("median surv:",medsurv),
      ref = par("usr")[2],
      winch = strwidth( paste0(pad.right, widest.events, pad.right), u="i"),
      cols=c("black",col)),
    getNamespace("graphics")
  )

	# number at risk, TODO: include the axis, automated number of ticks
	recordGraphics(
		{
    wdig <- strwidth("0",u="u")
		px <- par("xaxp")
		mtext( nrtitle, side=1, line=3, at=px[1] - wdig*(max.ndig-0.5) ,
				 	adj=0, cex = par("cex"))
    tis <- seq( px[1], min(px[2],xmax), (px[2]-px[1])/px[3])
    ntot <- rep(0,length(tis))
		for(i in 1:ngroup )
			{
			si <- if( ngroup > 1 ) S[i] else S
			for( j in 1:length(tis) )
				{
        ti <- tis[j]
				no <- summary( si, time=ti*xscale, extend=TRUE )$n.risk
				if(length(no))
          {
				 	mtext( no, at = ti + 0.5*wdig, side=1, line=3+i, adj=1,
								 col=col[i], cex = par("cex") )
				  ntot[j] <- ntot[j] + no
          }
        }
			}

    for( j in 1:length(tis) )
      {
      no <- ntot[j]
      ti <- tis[j]
      if( no > 0 )
        mtext( no, at = ti + 0.5*wdig, side=1, line=4+ngroup,adj=1,
          col="black",cex=par("cex"))
      }
		}
   ,
		list( nrtitle=nrtitle, ngroup=ngroup, S=S, 
				 xmax=xmax, col=col, xscale=xscale, max.ndig=max( nchar(n.risk0)) ),
		getNamespace("graphics")
	)


	# curve labels
	# if survival is tied at zero, break using the time where zero is reached
	t.s <- sapply( 1:ngroup,
					function(i) {
						Si <- if(ngroup > 1) S[i] else S
						ti <- min( xmax*xscale, max(Si$time) )
						c( ti, summary( Si, time=ti)$surv)
				 	})

  recordGraphics(
      {
      ppos <- nclust:::labelpack(ypos,
		    space = par("cxy")[2], 
        minx=ylim[1],maxx=ylim[2] )
      mtext(glab,side=4,at=ppos ,las=1,col=col,line=.5,cex=par("cex"))
      }, 
    list(glab=c(gtitle,glab), ypos = c(max(t.s[2,])+.05,t.s[2,] + 1e-12*t.s[1,]),
				 col=c("black",col), ylim=ylim ),
    getNamespace("graphics") )

  # compute Hazard ratios and confident intervals & Wald p-value
  # for selected comparisons
  L <- list()
  if( !is.null(HRcomparison) )
    {
    Sd <- survfit2data(S)
		HRtruncation <- xmax * xscale
    if( is.null(HRtext) )
      {
      HRtext <- sapply(HRcomparison,
        function( pair )
          {
          a <- pair[1]; b <- pair[2]
          if( a > ngroup || a < 1 ) return("")
          if( b > ngroup || b < 1 ) return("")
          tt <- c( Sd[[a]]$time, Sd[[b]]$time )
          dd <- c( Sd[[a]]$event, Sd[[b]]$event )
          dd <- ifelse( tt > HRtruncation, 0, dd )
          tt <- ifelse( tt > HRtruncation, HRtruncation, tt )
          xx <- c( rep(1,length(Sd[[a]]$time)), rep(0,length(Sd[[b]]$time) ))
          rr <- coxph( Surv(tt,dd) ~ xx )
          sc <- sumcox(rr)
          rownames(sc) <- paste0(glab[a]," vs ",glab[b])
          L[[paste0("(",a,",",b,")")]] <<- sc
          txt <- paste(
            ifelse(HRgroupcmp,
              paste(ifelse(HRgroupcmp==2,"\n",""),glab[a]," vs ",glab[b]),""),
            ifelse(HRestCI,
              paste(ifelse(HRestCI==2,"\n"," "),
                "HR=",
                format(exp(coef(rr)),digits=3),
                " [",
                format(exp(coef(rr)-1.96*sqrt(vcov(rr))),digits=3),
                ",",
                format(exp(coef(rr)+1.96*sqrt(vcov(rr))),digits=3),
                "]"), "" ),
            ifelse(HRpvalue,
              paste(ifelse(HRpvalue==2,"\n"," "),
                "p = ",
                format(2*pnorm(-abs(coef(rr)/sqrt(vcov(rr)))),
                  digits=2,scientific=5)), "" ),
            sep="" )
          } )

      # interaction terms, assume: odd == untreated, even == treated
      # fit in multivariable model with two factors: treatment (odd/even)
      # and everything else
      if( !is.null(HRint) && (ngroup > 2) && (ngroup %% 2 == 0))
        {
        survdat <- do.call("rbind",Sd)
        survdat$x <- as.factor(unlist(
          mapply(function(g,ng) rep(g %/% 2,ng),(1:ngroup)-1,lapply( Sd, nrow ))))
        survdat$z <- as.factor(unlist(
          mapply(function(g,ng) rep(g %% 2,ng),(1:ngroup)-1,lapply( Sd, nrow ))))
        coxint <- coxph(Surv(time,event) ~ x + z + x:z,data=survdat)
        nx <- length(table(survdat$x))-1
        trt <- paste0("(",gsub("^.*\\/","",glab[2]),
            " vs ",gsub("^.*\\/","",glab[1]),")")
        for(i in 1:nx)
          { 
          mrk <- paste0("(",gsub("\\/.*$","",glab[2*(i+1)])," vs ",
            gsub("\\/.*$","",glab[1]),")")
          ii <- nx+1+i
          est <- coef(coxint)[ii]
          sdest <- sqrt(vcov(coxint)[ii,ii])
          txtint <- paste0(
              " ", trt, " : ", mrk,"  RoHR = ",
              format(exp(est),digits=3),
              " [ ",
              format(exp(est-1.96*sdest),digits=3),
              ", ",
              format(exp(est+1.96*sdest),digits=3),
              " ] ",
              "p = ",format(2*pnorm(-abs(est/sdest)),digits=2,scientifi=5)
              )
          HRtext <- c(HRtext,txtint)
          }
        }
      }
    HRtext <- HRtext[sapply(HRtext,function(u)u!="")]
    nhr <- length(HRtext)
    for(i in 1:nhr )
      {
      mtext(HRtext[i],side = HRtextside, line= -i-(1-HRpadj),
             at=HRtextat,
            adj=0, padj = HRpadj,
						cex=par("cex"),font=1)
      }
    }
  if(!is.null(HRcomparison)) return(invisible(L))
}
