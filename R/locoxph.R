#
# local coxph
#

# The core function
#
locoxph <- function(
  tfu,   # follow-up time
  event,   # follow-up censoring indicator (1: event, 0: censoring)
  x,     # predictor 
	data = NULL, 		# additional context to lookup the first three variables
  weight = NULL,  # data point weights

  tgrid, # time grid (mandatory user supplied for the time being)

	trim = 0.025,   # discarded extremes for the gride (here 2.5%)
  xgrid = NULL ,  # test values to be predicted
	nx = 30,        # number of grid points

  sigmafrac = 1,  # Gaussian kernel width as fraction of the data SD
	sigma = NULL    # if specified, use it directly and ignore sigmafrac
  )
{
	attach(data)
	on.exit(detach(data),add=T)

  r <- c(quantile(x,trim,na.rm=T),quantile(x,1-trim,na.rm=T))
  if(is.null(xgrid))
    xgrid <- seq(r[1],r[2],(r[2]-r[1])/nx )
  
  nx <- length(xgrid)
  nt <- length(tgrid)
	
  if(is.null(sigma))
    sigma = sigmafrac * sd(x,na.rm=T)

	surv <- aperm(
				array(  
					unlist(sapply( xgrid, function(xc){
						w <- exp( -0.5*( (x-xc)/sigma )^2 )
						w <- ifelse( w < 1e-12, 1e-12, w )
						if(!is.null(weight)) w <- w * weight
						summary(
							survfit(
								coxph( Surv(tfu, event) ~ x, weights=w ),
								newdata = c(x=xc) ),
							times = tgrid )[c("surv","lower","upper")]
						}))
				 ,dim=c(nt,3,nx)
				 ),
				c(3,2,1))

  out <- array( sapply(1:nt, function(i){cbind( xgrid, surv[,,i])}),
								dim=c(nx,4,nt),
    						dimnames=list( NULL,c(deparse(substitute(x)),
															"surv","lower","upper"),time=tgrid))

	# pass this on for use by nplot.locoxph
	attr(out,"x") <- x

  class(out) <- "locoxph"
  out
}


