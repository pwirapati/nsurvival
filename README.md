# nsurvival
Extensions to R survival package

## Installation

Requires the package `survival` and `nclust` to be installed. The former should be available by default in any R installation. The latter is from  https://gitlab.com/pwirapati/nclust (only needed for the `labelpack` function, which in the future should be included in `nsurvival` or more generic graphics extension package).

```
devtools::install_gitlab("pwirapati/nsurvival")
```
