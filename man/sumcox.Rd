\name{sumcox}
\alias{sumcox}
\alias{forest_sumcox}
\title{Forest plot summary of Cox regression}
\description{Summarize Cox regression results and display as forest plots}
\usage{
  sumcox( fit, terms = NULL )
  forest_sumcox (S, 
    hrlim = NULL,
    pval_cut = 0.05, fade_ns = 1, ngap = 1, 
    xlab = "HR", col.var.mod = NULL,
    lmar = 20, cex = 2, pch = 20, 
    lwd = 2) 
}
\arguments{
  \item{fit}{Object returned by \code{coxph}}
  \item{terms}{List of terms (variable names) to show}
  \item{S}{List of \code{sumcox} output}
  \item{hrlim}{Range of hazard ratio axis}
  \item{pval_cut}{The p-value cutoff for showing in faded colors when not
    significant}
  \item{fade_ns}{Degree of fading: 1: no fading (original color), 0: invisible}
  \item{ngap}{Gaps between different fit results}
  \item{xlab}{x-axis label}
  \item{col.var.mod}{Modifier of color for displaying variables. The default
    is black. This given as a vector of colors, named by the regular
    expression that is matched against the variable names}
  \item{lmar}{Left margin width (in "lines")}
  \item{cex}{Point size of the dots showing the estimates}
  \item{pch}{Point type of the dots showing the estimates}
  \item{lwd}{Confidence bar line width}
}
